const Cart = require("../models/Cart");
const Product = require("../models/Product");
const User = require("../models/User");


// adding products to cart (CREATING A CART)
module.exports.addToCart = async (data) => {
	if (data.isAdmin) {
		return Promise.resolve({ value: `This feature is not available to you.` });
	} else {
		const productsArray = data.products;
		const productSpec = [];
		let newCart = {
			userId: data.userId,
			products: productSpec
		};

		let currentTotalAmount = 0;

		for (const product of productsArray) {
			const selectedProduct = await Product.findById(product.productId);
		
			const totalForThisProduct = selectedProduct.price * product.quantity; 
		
			const pushToProductsArray = {
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: product.quantity,
				subTotal: totalForThisProduct
			};
			
			currentTotalAmount += totalForThisProduct; // accumulate the total
			
			newCart.products.push(pushToProductsArray);
		}

		newCart = new Cart({
			userId: data.userId,
			products: newCart.products,
			totalAmount: currentTotalAmount
		});

		const cart = await newCart.save();

		if (cart) {
			const cartCreated = `Cart successfully created! The current total amount of the item(s) in your cart is ${currentTotalAmount}.`;
			return cartCreated;
		} else {
			return false;
		}
	}
};

/*==========[STRETCH GOALS]=============*/

// retrieve authenticated user's cart
module.exports.retrieveCart = (reqParams) => {
	return Cart.findById(reqParams.cartId).then(result => {
		return result;
	});
};

// retrieve ALL carts (ADMIN only)
module.exports.retrieveAllCart = (data) => {
	if (data.isAdmin) {
		return Cart.find({}).then(result => {return result; });
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return value;
	});
};

// change product's quantity inside a cart || add/remove product(s)
module.exports.changeQuantity = async (reqParams, data) => {
	if (data.isAdmin) {
		return Promise.resolve({ value: `This feature is not available to you.` });
	} else {
		const productsArray = data.products;
		const productSpec = [];
		let newQuantity = {
			userId: data.userId,
			products: productSpec
		};

		let currentTotalAmount = 0;

		for (const product of productsArray) {
			const selectedProduct = await Product.findById(product.productId);
		
			const totalForThisProduct = selectedProduct.price * product.quantity; 
		
			const pushToProductsArray = {
				productId: selectedProduct._id,
				name: selectedProduct.name,
				description: selectedProduct.description,
				price: selectedProduct.price,
				quantity: product.quantity,
				subTotal: totalForThisProduct
			};
			
			currentTotalAmount += totalForThisProduct; // accumulate the total
			newQuantity = {
				userId: data.userId,
				products: productSpec,
				totalAmount: currentTotalAmount
			};

			newQuantity.products.push(pushToProductsArray);
		}

		return Cart.findByIdAndUpdate(reqParams.cartId, newQuantity).then((cart, error) => {
			if (error) {
				return false;
			} else {
				let cartUpdated = `The cart has been successfully updated. You new current total is ₱${currentTotalAmount}.`
				return cartUpdated;
			};
		});
	}
};


