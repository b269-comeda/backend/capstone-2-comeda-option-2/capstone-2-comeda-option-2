const Product = require("../models/Product");

// creating product (ADMIN ONLY)
module.exports.createProduct = (data) => {
	if (data.isAdmin) {
		let newProduct = new Product ({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price 
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				let productCreatedMessage = `Product successfully added to the database!`
				return productCreatedMessage;
			};
		});
	};
	let notAdminMessage = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return notAdminMessage.then((value) => {
		return {value};
	});
	
};

// retrieving all ACTIVE products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

// retrieve a single specific product
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// updating product information (ADMIN only)
module.exports.updateProductInfo = (reqParams, data) => {
	if (data.isAdmin) {
		let updatedProduct = {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		};
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				let productUpdatedMessage = `The product's information has been successfully updated.`
				return productUpdatedMessage;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
};

// archiving product (ADMIN only)
module.exports.archiveProduct = (reqParams, data) => {
	if (data.isAdmin) {
		let archivedProduct = {
			isActive: false
		};
		return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
			if (error) {
				return false;
			} else {
				let productArchivedMessage = `The product has been successfully archived.`
				return productArchivedMessage;
			};
		});
	};
	let unauthorizedAccess = Promise.resolve(`Unauthorized access. This feature is not available to you.`);
	return unauthorizedAccess.then((value) => {
		return {value};
	});
};




