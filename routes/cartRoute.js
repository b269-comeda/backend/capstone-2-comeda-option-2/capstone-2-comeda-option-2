const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const cartController = require("../controllers/cartController");

const auth = require("../auth");


// route for adding products to cart (CREATING A CART)
router.post("/add-to-cart", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
});


/*==========[STRETCH GOALS]=============*/


// route for retrieving authenticated user's cart 
router.get("/:cartId", (req, res) => {
	cartController.retrieveCart(req.params).then(resultFromController => res.send(resultFromController));
});

// route for retrieving ALL carts (ADMIN only)
router.get("/all/createdcarts", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	cartController.retrieveAllCart(data).then(resultFromController => res.send(resultFromController));
});

// route for changing the product's quantity inside a cart || add/remove product(s)
router.patch("/change-quantity/:cartId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		products: req.body.products
	}
	cartController.changeQuantity(req.params, data).then(resultFromController => res.send(resultFromController));
});

module.exports = router; // PUT YOUR CODE ABOVE THIS LINE, JEH



